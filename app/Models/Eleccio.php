<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\EleccionsController;

class Eleccio extends Model
{
    use HasFactory;

    protected $fillable = [
        'codi_municipi',
        'nom_municipi',
        'nombre_total_meses',
        'cens_avancament_1',
        'participacio_avancament_1',
    ];
    public $timestamps = false;
    protected $table = 'eleccions'; 
}
