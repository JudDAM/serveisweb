<?php

namespace App\Http\Controllers;
use App\Models\Eleccio;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\EleccionsImport;
use Illuminate\Http\Request;

class EleccionsController extends Controller
{
    public function eleccionsImport()
    {
        Excel::import(new EleccionsImport, 'eleccions.csv');
    }

    public function index(Request $request)
    {
        $eleccio = Eleccio::all();
        return $eleccio;
    }
   
    public function store(Request $request)
    {
        $eleccio = new Eleccio();
        $eleccio->codi_municipi = $request->codi_municipi;
        $eleccio->nom_municipi = $request->nom_municipi;
        $eleccio->nombre_total_meses = $request->nombre_total_meses;
        $eleccio->cens_avancament_1 = $request->cens_avancament_1;
        $eleccio->participacio_avancament_1 = $request->participacio_avancament_1;

        $eleccio->save();

        return response()->json([
            'message' => "Eleccio almacenada con éxito"
        ], 201);
    }

    
    public function show(Request $request)
    {
       // $eleccio = Eleccio::where('codi_municipi',$codigo)->get();

       $eleccio = Eleccio::findOrFail($request->id);

        return $eleccio;
    }
    
  
    public function update(Request $request)
    {
        //$eleccio = Eleccio::where('codi_municipi',$codigo)->get();
        $eleccio = Eleccio::findOrFail($request->id);

        $eleccio->codi_municipi = $request->codi_municipi;
        $eleccio->nom_municipi = $request->nom_municipi;
        $eleccio->nombre_total_meses = $request->nombre_total_meses;
        $eleccio->cens_avancament_1 = $request->cens_avancament_1;
        $eleccio->participacio_avancament_1 = $request->participacio_avancament_1;

        $eleccio->save();

        return $eleccio;
    }

   
    public function destroy(Request $request)
    {
        $eleccio = Eleccio::destroy($request->id);
        return response()->json([
            'message' => "Eleccio con id = " . $eleccio . " ha sido borrado con éxito"
        ], 201);
    }
}
