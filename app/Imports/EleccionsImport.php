<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Models\Eleccio;

class EleccionsImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $row) 
        {
            if($row[0] != 'codi_municipi') {
                $ok = Eleccio::create ([
                    'codi_municipi' => $row[0],
                    'nom_municipi' => $row[1],
                    'nombre_total_meses' => $row[2],
                    'cens_avancament_1' => $row[3],
                    'participacio_avancament_1' => $row[4],
                ]);
            }
        }
    }

    public function chunkSize(): int 
    {
        return 1000;
    }
}
