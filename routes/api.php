<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Contracts\Cache\Store;
use App\Http\Controllers\EleccionsController;
use App\Models\Eleccio;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/eleccions', [EleccionsController::class, 'index']);
Route::put('/eleccions/actualizar/{id}', [EleccionsController::class, 'update']);
Route::post('/eleccions/guardar', [EleccionsController::class, 'store']);
Route::delete('/eleccions/borrar/{id}', [EleccionsController::class, 'destroy']);
Route::get('/eleccions/buscar/{id}', [EleccionsController::class, 'show']);
