<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class EleccionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('eleccions')->insert([
            'codi_municipi' => Str::random(10),
            'nom_municipi' => Str::random(10),
            'nombre_total_meses' => Str::random(10),
            'cens_avancament_1' => Str::random(10),
            'participacio_avancament_1' => Str::random(10),
        ]);
    }
}
